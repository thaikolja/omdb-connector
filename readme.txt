=== OMDb Connector ===

Contributors:       thaikolja
Donate link:        https://www.paypal.me/thaikolja
Tags:               omdb, imdb, movies, films
Requires at least:  5.0
Tested up to:       5.5
Stable tag:         trunk
Requires PHP:       7.1
License:            GPL v2 or later
License URI:        https://www.gnu.org/licenses/gpl-2.0.txt

Stable: 1.0.0

== Description ==

sadad

== Installation ==

1. Download and activate the OMDb Connector plugin
2. Use shortcodes, PHP classes and functions to use movie details in your site
3. Write a quick review on [WordPress.org](https://wordpress.org/support/plugin/omdb-connector/reviews/#new-post)

**Please see the [full documentation](#) to learn how to use the plugin and its features.**

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.


== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif).
2. This is the second screen shot

== Changelog ==

1.0.0
Initial release
== Upgrade Notice ==

=== 1.0.0 ===
* This is the first public version of OMDb Connector.