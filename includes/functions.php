<?php
/**
 * Copyright (C) 2020 by Kolja Nolte
 * https://www.koljanolte.com
 * kolja.nolte@gmail.com
 *
 * This theme is licensed under Creative Commons BY-NC 4.0.
 * You are free to share, copy, and redistribute the material
 * in any medium or format. You may adapt, remix, transform,
 * and build upon the material. You must give appropriate credit,
 * provide a link to the license, and indicate if changes were made.
 * You may not use the material for commercial purposes.
 *
 * @package OMDb Connector
 */

/** Prevents this file from being called directly */
if (!function_exists("add_action")) {
    return;
}

/**
 * @return string
 */
function omdb_connector_get_plugin_version(): string {
    return (new OMDb_Connector_Init())->plugin_version;
}

/**
 * @param string $movie_id
 *
 * @return \OMDb_Movie
 */
function omdb_connector_get_movie(string $movie_id): OMDb_Movie {
    $class = new OMDb_Connector();

    return $class->get_movie($movie_id);
}