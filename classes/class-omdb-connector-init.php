<?php
/**
 * Copyright (C) 2020 by Kolja Nolte
 * https://www.koljanolte.com
 * kolja.nolte@gmail.com
 *
 * This theme is licensed under Creative Commons BY-NC 4.0.
 * You are free to share, copy, and redistribute the material
 * in any medium or format. You may adapt, remix, transform,
 * and build upon the material. You must give appropriate credit,
 * provide a link to the license, and indicate if changes were made.
 * You may not use the material for commercial purposes.
 *
 * @package OMDb Connector
 */

/**
 * Class OMDb_Connector_Init
 */
class OMDb_Connector_Init {
    /**
     * @var string
     */
    public $plugin_url = "";

    /**
     * @var string
     */
    public $plugin_path = "";

    /**
     * @var string
     */
    public $plugin_version = "";

    /**
     * OMDb_Connector_Init constructor.
     */
    public function __construct() {
        define("TEXTDOMAIN", "omdb-connector"); // Todo: Replace and remove before publishing
        define("OMDB_CONNECTOR_TEXTDOMAIN", "omdb-connector");
        define("OMDB_CONNECTOR_VERSION", "0.1.0");

        $this->plugin_url     = plugins_url("", __DIR__);
        $this->plugin_version = OMDB_CONNECTOR_VERSION;

        add_action("admin_enqueue_scripts", [$this, "load_admin_styles"]);
        add_action("admin_enqueue_scripts", [$this, "load_admin_scripts"]);
        add_action("admin_menu", [$this, "register_admin_pages"]);
        add_action("admin_init", [$this, "install"]);
    }

    /**
     * @return bool|false|int
     */
    public function install() {
        global $wpdb;

        $table_name = $wpdb->prefix . "omdb_connector_movies";

        /** Check if database table exists */
        $query = "CREATE TABLE IF NOT EXISTS `$table_name` (
                id              INT(5) PRIMARY KEY AUTO_INCREMENT NOT NULL,
                movie_title     TEXT,
                movie_year      INT(4),
                movie_genre     LONGTEXT,
                movie_rated     TEXT,
                movie_released  TEXT
            );
         ";

        // TODO: Add all details to the query
        //require_once ABSPATH . "/wp-admin/includes/upgrade.php";

        return $wpdb->query($query);
    }

    /**
     * Load CSS styles only for the plugin's
     * admin page defined below.
     *
     * @since 1.0.0
     */
    public function load_admin_styles(): void {
        $styles_url     = $this->plugin_url . "/styles/css";
        $current_screen = (object)get_current_screen();

        if ($current_screen->id !== "tools_page_omdb-connector") {
            return;
        }

        wp_enqueue_style(
            "omdb-connector-styles-admin",
            $styles_url . "/admin.min.css",
            [],
            $this->plugin_version
        );

        wp_enqueue_style(
            "omdb-connector-styles-bootstrap",
            $styles_url . "/bootstrap.min.css",
            ["omdb-connector-styles-admin"],
            "4.4.1"
        );

        wp_enqueue_style(
            "omdb-connector-styles-font-awesome",
            $styles_url . "/font-awesome.min.css",
            ["omdb-connector-styles-admin"],
            "5.12.0"
        );
    }

    public function load_admin_scripts(): void {
        $scripts_url    = $this->plugin_url . "/scripts/js";
        $current_screen = (object)get_current_screen();

        if ($current_screen->id !== "tools_page_omdb-connector") {
            return;
        }

        wp_enqueue_script(
            "omdb-connector-scripts-admin",
            $scripts_url . "/main.min.js",
            ["jquery"],
            $this->plugin_version,
            true
        );
    }

    /**
     * Add the main admin page for this plugin
     *
     * @since 1.0.0
     */
    public function register_admin_pages(): void {
        add_submenu_page(
            "tools.php",
            "OMDb Connector",
            "OMDb Connector",
            "manage_options",
            "omdb-connector",
            "page_omdb_connector_admin"
        );
    }
}
