<?php
/**
 * Copyright (C) 2020 by Kolja Nolte
 * https://www.koljanolte.com
 * kolja.nolte@gmail.com
 *
 * This theme is licensed under Creative Commons BY-NC 4.0.
 * You are free to share, copy, and redistribute the material
 * in any medium or format. You may adapt, remix, transform,
 * and build upon the material. You must give appropriate credit,
 * provide a link to the license, and indicate if changes were made.
 * You may not use the material for commercial purposes.
 *
 * @package OMDb Connector
 */

/**
 * Class OMDb_Connector
 */
class OMDb_Connector {
    /** The API key we're using */
    private $omdb_api_key = "9d1d8547";

    /** Plugin settings */
    public $settings = [];

    /**
     * OMDb_Connector constructor.
     */
    public function __construct() {
        /** Get all stored settings */
        $settings      = get_option("omdb_connector_settings");
        $setting_names = [
            "version"
        ];

        foreach ($setting_names as $setting_name) {
            $settings[] = get_option("omdb_connector_" . $setting_name);
        }

        /** Add them to $this->settings */
        $this->settings = (array)$settings;
    }

    public function get_movie_api_type(string $movie_id): string {
        $movie_api_type = "t";

        if ($movie_id[0] === "t" && $movie_id[1] === "t" && strlen($movie_id) === 9) {
            $movie_api_type = "i";
        }

        return $movie_api_type;
    }

    public function get_movie_details_scheme() {
        $scheme = [
            "title",
            "year",
            "genre",
            "rated",
            "released"
        ];

        return $scheme;
    }

    /**
     * @param string $movie_id
     *
     * @return bool
     */
    public function cache_movie(string $movie_id) {
        global $wpdb;

        $table         = $wpdb->prefix . "omdb_connector_movies";
        $format        = [];
        $values        = [];
        $movie_details = $this->get_movie_data($movie_id);

        if (!isset($movie_details["title"])) {
            return false;
        }

        foreach ($this->get_movie_details_scheme() as $detail) {
            $value = $movie_details[$detail];

            if (is_array($value)) {
                $value = serialize($value);
            }

            $values["movie_$detail"] = $value;
        }

        foreach ($values as $db_key => $value) {
            $type = "%s";

            if (is_int($value)) {
                $type = "%d";
            }
            elseif (is_float($value)) {
                $type = "%f";
            }

            $format[] = $type;
        }

        $wpdb->insert($table, $values, $format);

        return (bool)$wpdb->insert_id;
    }

    /**
     * @param string $movie_id
     * @param array  $parameters
     *
     * @return array
     */
    public function get_movie_data(string $movie_id, array $parameters = []): array {
        // TODO: Get cached movie data if exists

        /** The default URL we will build upon */
        $base_url = "https://www.omdbapi.com/?";

        /** Default parameters */
        $default_parameters = [
            "apikey" => $this->omdb_api_key,
        ];

        /** ... */
        $parameters[$this->get_movie_api_type($movie_id)] = $movie_id;

        /** Merge the default parameters and the custom ones */
        $parameters = (array)wp_parse_args($parameters, $default_parameters);

        /** Convert parameters into a string */
        $api_url = $base_url . build_query($parameters);

        /** Add a filter to allow developers to override or change the URL */
        $api_url = apply_filters("omdb_connector_api_url", $api_url);

        /** The raw data obtained from the API call */
        $movie_data_raw = wp_safe_remote_get($api_url);

        /** But we need only the body which will come as a JSON string */
        $movie_data_body = wp_remote_retrieve_body($movie_data_raw);

        /** Turn the JSON string, i.e. movie details, into an array */
        $movie_data_encoded = json_decode($movie_data_body, true);

        /** Some cosmetic changes; make all movie details keys lowercase */
        $movie_data = array_change_key_case($movie_data_encoded, CASE_LOWER);

        /** Filter for the actual movie data */
        $movie_data = apply_filters("omdb_connector_movie_data", $movie_data);

        $movie_data = $this->sanitize_movie_data($movie_data);

        // TODO: Check if caching is on and add movie to the cache

        return $movie_data;
    }

    /**
     * @param array $movie_data
     *
     * @return array
     */
    public function sanitize_movie_data(array $movie_data): array {
        if (count($movie_data) <= 0) {
            return [];
        }

        foreach ($movie_data as $key => $value) {
            if ($key === "actors" || $key === "writer" || $key === "genre" || $key === "language") {
                $new_movie_data   = explode(", ", $value);
                $movie_data[$key] = $new_movie_data;

            }//11. 6.1993
            if ($key === "released") {
                $timestamp              = strtotime($value);
                $movie_data["released"] = [
                    "year"      => date("Y"),
                    "month"     => date("n"),
                    "day"       => date("j"),
                    "timestamp" => $timestamp,
                ];
            }

            // TODO: Rewrite imdbvotes and boxoffice into proper format

            /*if ($key === "imdbvotes" || $key === "boxoffice") {
                $sanitized_number = sanitize_key($value);

                if (is_int($sanitized_number)) {
                    $movie_data[$key] = number_format($sanitized_number);
                }

            }*/
        }

        return $movie_data;
    }

    /**
     * @param string $movie_id
     * @param array  $options
     *
     * @return \OMDb_Movie
     */
    public function get_movie(string $movie_id, array $options = []): OMDb_Movie {
        /** An empty OMDb Movie object that we have to fill up */
        $movie = new OMDb_Movie($movie_id, $options);

        /** Go through each movie detail and add it to our empty object */
        foreach ($this->get_movie_data($movie_id) as $name => $value) {
            $movie->$name = $value;
        }

        /** Filter for the actual movie */
        $movie = apply_filters("omdb_connector_movie_{$movie_id}", $movie);

        return $movie;
    }
}
