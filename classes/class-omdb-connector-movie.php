<?php
/**
 * Copyright (C) 2020 by Kolja Nolte
 * https://www.koljanolte.com
 * kolja.nolte@gmail.com
 *
 * This theme is licensed under Creative Commons BY-NC 4.0.
 * You are free to share, copy, and redistribute the material
 * in any medium or format. You may adapt, remix, transform,
 * and build upon the material. You must give appropriate credit,
 * provide a link to the license, and indicate if changes were made.
 * You may not use the material for commercial purposes.
 *
 * @package OMDb Connector
 */

/** Prevents this file from being called directly */
if (!function_exists("add_action")) {
    return;
}

/**
 * Class OMDb_Movie
 */
class OMDb_Movie {
    /**
     * @var string
     */
    public $title = "";

    /**
     * @var int
     */
    public $year = 0;

    /**
     * @var string
     */
    public $rated = "";

    /**
     * @var int
     */
    public $released = 0;

    /**
     * @var array
     */
    public $runtime = [];

    /**
     * @var array
     */
    public $genres = [];

    /**
     * @var array|mixed
     */
    public $languages = [];

    /**
     * OMDb_Movie constructor.
     *
     * @param       $movie_name_or_id
     * @param array $options
     */
    function __construct($movie_name_or_id, array $options = []) {
        $class      = new OMDb_Connector();
        $movie_data = $class->get_movie_data($movie_name_or_id);
    }
}