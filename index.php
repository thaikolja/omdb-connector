<?php
/**
 * Copyright (C) 2020 by Kolja Nolte
 * https://www.koljanolte.com
 * kolja.nolte@gmail.com
 *
 * This theme is licensed under Creative Commons BY-NC 4.0.
 * You are free to share, copy, and redistribute the material
 * in any medium or format. You may adapt, remix, transform,
 * and build upon the material. You must give appropriate credit,
 * provide a link to the license, and indicate if changes were made.
 * You may not use the material for commercial purposes.
 *
 * @package OMDb Connector
 */

/** Prevents this file from being called directly */
if (!function_exists("add_action")) {
    return;
}

/** Nothing's going on here, just in case Apache's DirectoryListing is on */
