<?php
/**
 * Copyright (C) 2020 by Kolja Nolte
 * https://www.koljanolte.com
 * kolja.nolte@gmail.com
 *
 * This theme is licensed under Creative Commons BY-NC 4.0.
 * You are free to share, copy, and redistribute the material
 * in any medium or format. You may adapt, remix, transform,
 * and build upon the material. You must give appropriate credit,
 * provide a link to the license, and indicate if changes were made.
 * You may not use the material for commercial purposes.
 *
 * @package OMDb Connector
 */

/** Prevents this file from being called directly */
if (!function_exists("add_action")) {
    return;
}

/**
 * Plugin Name:         OMDb Connector
 * Plugin URI:          https://www.koljanolte.com/wordpress/plugins/omdb-connector
 * Description:         A versatile plugin to fetch movie details from the OMDb API.
 * Version:             0.1.0
 * Requires at least:   5.0
 * Requires PHP:        7.1
 * Author:              Kolja Nolte
 * Author URI:          https://www.koljanolte.com
 * Text Domain:         omdb-connector
 * License:             GPL v2 or later
 * License URI:         https://www.gnu.org/licenses/gpl-2.0.txt
 */

/** A list of folders whose .php files are being loaded */
$include_directories = [
    "admin",
    "classes",
    "includes",
    "pages"
];

/** Loop through the set directories */
foreach ($include_directories as $include_directory) {
    /** Define the plugin's main path */
    $include_directory = plugin_dir_path(__FILE__) . $include_directory;

    /** Skip directory if it's not a valid directory */
    if (!is_dir($include_directory)) {
        continue;
    }

    /** Gather all .php files within the current directory */
    $include_files = glob($include_directory . "/*.php");

    foreach ($include_files as $include_file) {
        /** Skip file if file is not valid */
        if (!is_file($include_file)) {
            continue;
        }

        /** Sanitize the file name */
        $include_file = str_replace(
            basename($include_file),
            sanitize_file_name(basename($include_file)),
            $include_file
        );

        require_once $include_file;
    }
}

/** Initialize the plugin */
new OMDb_Connector_Init();

register_activation_hook(__FILE__, ["OMDb_Connector_Init", "install"]);